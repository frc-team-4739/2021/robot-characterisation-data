import csv as csv_module
import json
import math

csvs = {
    "fast-backward": "StaticBackward.csv",
    "fast-forward": "StaticForward.csv",
    "slow-backward": "DynamicBackward.csv",
    "slow-forward":"DynamicForward.csv"
}

data = {
    "sysid": True,
    "test": "Simple",
    "units": "Radians",
    "unitsPerRotation": math.pi * 2,
}

for (test, csv_name) in csvs.items():
    with open(csv_name, newline="") as csvfile:
        csv = csv_module.reader(csvfile)
        next(csv)
        data[test] = []
        for row in csv:
            time = float(row[1])
            voltage = float(row[3])
            position = float(row[0])
            velocity = float(row[2])
            data[test].append([time, voltage, position, velocity])

with open("output.json", "w") as f:
    json.dump(data, f)
            

